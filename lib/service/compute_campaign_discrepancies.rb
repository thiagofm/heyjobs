module HeyJobs
  module Service
    class ComputeCampaignDiscrepancies
      MAPPING = {
        status: "status",
        external_reference: "reference",
        ad_description: "description"
      }

      def initialize(remote_campaign, local_campaign)
        @remote_campaign = remote_campaign
        @local_campaign = local_campaign
      end

      def call
        {
          remote_reference: remote_reference,
          discreprancies: discrepancies
        }
      end

      private
      attr_reader :remote_campaign, :local_campaign

      def remote_reference
        remote_campaign["reference"]
      end

      def discrepancies
        MAPPING.inject([]) do |discrepancies_list, (local_key, remote_key)|
          if local_campaign
            local_value = local_campaign[local_key]
          else
            # When local campaign is empty, we let the local value as "nil"
            # so later we might want to save in the DB those which are set as nil
            local_value = nil
          end

          remote_value = remote_campaign[remote_key] 

          if local_value != remote_value
            discrepancies_list.push(
              {
                remote_key => {
                  "remote" => remote_value,
                  "local" => local_value
                }
              }
            )
          end

          discrepancies_list
        end
      end

      class << self
        def call(*args)
          new(*args).()
        end
      end
    end
  end
end