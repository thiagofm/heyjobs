require 'net/http'
require 'json'

module HeyJobs
  module Client
    class Campaigns
      URL = 'https://mockbin.org/bin/fcb30500-7b98-476f-810d-463a0b8fc3df'

      def initialize(url = URL)
        @url = url
      end

      def response
        JSON.parse(response_body)["ads"]
      end

      private
      attr_reader :url

      def response_body
        Net::HTTP.get(URI(url))
      end
    end
  end
end