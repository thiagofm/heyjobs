require 'dotenv/load'

Dir.glob("lib/**/*.rb").each do |file|
  require_relative "../#{file}"
end

