module HeyJobs
  module Service
    class ImportCampaigns
      def initialize
      end

      def call
        remote_campaigns.map do |remote_campaign|
          # Doing that over here so we avoid n+1's
          local_campaign = local_campaigns.find {|local_campaign| local_campaign[:external_reference] == remote_campaign["reference"] }

          HeyJobs::Service::ComputeCampaignDiscrepancies.(remote_campaign, local_campaign)
        end
      end

      private
      attr_reader :response

      def remote_campaigns
        @_remote_campaigns ||= HeyJobs::Client::Campaigns.new.response
      end

      def remote_external_references
        @_remote_external_references ||= remote_campaigns.map {|campaign| campaign["reference"] }
      end

      def local_campaigns
        @_local_campaigns ||= DB[:campaigns].where(external_reference: remote_external_references).to_a
      end

      class << self
        def call(*args)
          new(*args).()
        end
      end
    end
  end
end