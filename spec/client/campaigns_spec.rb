require 'spec_helper'

describe HeyJobs::Client::Campaigns do
  subject(:campaigns) { described_class.new }

  let(:campaigns_fixture) { File.read("spec/fixtures/campaigns.json") }
  let(:campaigns_json) { JSON.parse(campaigns_fixture) }

  before do
    # Would use webmock, but for the sake of not using another gem then
    # just mocked the private Campaigns#response_body method, which does the job
    allow(campaigns).to receive(:response_body).and_return(campaigns_fixture)
  end

  it "can successfully request the remote campaigns" do
    expect(campaigns.response).to eq(campaigns_json["ads"])
  end
end