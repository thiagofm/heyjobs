require_relative "../lib/heyjobs.rb"

Dotenv.overload('.env.test')

RSpec.configure do |config|
  config.formatter = 'documentation'
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.before(:all) do
    DB.drop_table(:campaigns) if DB.tables.include?(:campaigns)
    DB.create_table :campaigns do
      primary_key :id
      Integer :job_id
      String :status
      String :external_reference
      String :ad_description
    end
  end
end
