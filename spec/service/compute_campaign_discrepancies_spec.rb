require 'spec_helper'

describe HeyJobs::Service::ComputeCampaignDiscrepancies do
  subject(:import_campaigns) { described_class.(remote_campaign, local_campaign) }

  let(:local_campaign) do
    {
      job_id: 1,
      status: "enabled",
      external_reference: "1",
      ad_description: "Description for campaign 11"
    }
  end

  let(:remote_campaign) do
    {
      "reference" => "1",
      "status" => "disabled",
      "description" => "description for the new campaign"
    }
  end

  it "finds the discrepancies betwen the local and remote campaigns" do
    expect(import_campaigns).to eq(
      {
        :remote_reference=>"1",
        :discreprancies=> [
          {"status"=>{"remote"=>"disabled", "local"=>"enabled"}},
          {"description"=>{"remote"=>"description for the new campaign", "local"=>"Description for campaign 11"}}
        ]
      }
    )
  end
end