require 'spec_helper'

describe HeyJobs::Service::ImportCampaigns do
  subject(:import_campaigns) { described_class.() }

  context "diffs local campaigns with remote (integration)", integration: true do
    before do
      DB[:campaigns].insert(
        job_id: 1,
        status: "enabled",
        external_reference: "1",
        ad_description: "Description for campaign 11"
      )

      DB[:campaigns].insert(
        job_id: 2,
        status: "enabled",
        external_reference: "2",
        ad_description: "Description for campaign 12"
      )
    end

    it "has no discrepancies for the first record" do
      expect(import_campaigns[0]).to eq(
        {:remote_reference=>"1", :discreprancies=>[]}
      )
    end

    it "has discrepancies on the status for the second record" do
      expect(import_campaigns[1]).to eq(
        {:remote_reference=>"2",
          :discreprancies=>
          [{"status"=>{"remote"=>"disabled", "local"=>"enabled"}}]}
      )
    end

    it "has discrepancies on all fields for the third record, as it has never been saved before" do
      expect(import_campaigns[2]).to eq(
        {:remote_reference=>"3",
          :discreprancies=>
          [{"status"=>{"remote"=>"enabled", "local"=>nil}},
            {"reference"=>{"remote"=>"3", "local"=>nil}},
            {"description"=>{"remote"=>"Description for campaign 13", "local"=>nil}}]})
    end
  end
end